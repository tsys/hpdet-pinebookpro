#!/usr/bin/env sh

set -x

install -D -m644 audio_jack_plugged_in "$INSTALL_PREFIX"/etc/acpi/events/audio_jack_plugged_in
install -D -m755 audio_jack_plugged_in.sh "$INSTALL_PREFIX"/etc/acpi/audio_jack_plugged_in.sh
install -D -m755 sync.sh "$INSTALL_PREFIX"/usr/share/hpdet-pinebookpro/sync.sh
install -D -m644 hpdet-pinebookpro.service "$INSTALL_PREFIX"/usr/lib/systemd/system/hpdet-pinebookpro.service
