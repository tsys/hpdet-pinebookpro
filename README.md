Pinebook Pro speaker control
============================

This tool utilizes acpid to disable/enable the Pinebook Pros speakers when headphones are plugged/unplugged.

# Installation

## Arch based systems (e.g. Manjaro)

```
sudo pacman -S acpid
sudo systemctl enable --now acpid
sudo ./install.sh
sudo systemctl restart acpid
```

## Generic

1. Ensure you have acpid installed and the acpid service is enabled.
2. Run install.sh (as root!). 
3. Restart acpid.
